package com.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dto.Register;

public interface RegisterRepository extends JpaRepository<Register, Integer>{
	
	@Query("from Register r where r.FirstName = :FirstName")
	public Register getRegisterByfirstName(@Param("FirstName") String FirstName);
	
	@Query("from Register r where r.EmailId = :EmailId and r.Password =:Password")
	public Register login(@Param("EmailId") String EmailId, @Param("Password") String Password);
}
